package me.piomar.dataflow;

import java.util.Map;

import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;

public interface Flow {

    Map<String, NetworkDetails> fetchNetworks(Envelope scope);
}
