package me.piomar.dataflow;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.time.DurationFormatUtils;

import com.google.common.base.Preconditions;

import hu.akarnokd.rxjava3.debug.RxJavaAssemblyTracking;
import io.reactivex.rxjava3.exceptions.UndeliverableException;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import me.piomar.dataflow.model.Envelope;

public class Utils {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IllegalStateException(e);
        }
    }

    public static double[] linspace(double start, double stop, final int count) {
        Preconditions.checkArgument(count >= 2, "Number of points should be at least 2");
        double[] result = new double[count];
        result[0] = start;
        result[count - 1] = stop;
        for (int i = 1; i < count - 1; i++) {
            result[i] = start + i * (stop - start) / (count - 1);
        }
        return result;
    }

    public static List<Envelope> envelopeSplit(Envelope envelope, double maxTileSize) {
        double diffX = Math.abs(envelope.maxX() - envelope.minX());
        double diffY = Math.abs(envelope.maxY() - envelope.minY());
        int tilesX = (int) Math.ceil(diffX / maxTileSize);
        int tilesY = (int) Math.ceil(diffY / maxTileSize);

        double[] gridPointsX = linspace(envelope.minX(), envelope.maxX(), tilesX + 1);
        double[] gridPointsY = linspace(envelope.minY(), envelope.maxY(), tilesY + 1);

        List<Envelope> grid = new ArrayList<>(tilesX * tilesY);
        for (int y = 0; y < tilesY; y++) {
            for (int x = 0; x < tilesX; x++) {
                grid.add(new Envelope(gridPointsX[x], gridPointsX[x + 1], gridPointsY[y], gridPointsY[y + 1]));
            }
        }
        return grid;
    }

    public static String formatDuration(Duration duration) {
        return DurationFormatUtils.formatDuration(duration.toMillis(), "H:mm:ss.SSS");
    }

    /**
     * https://github.com/ReactiveX/RxJava/wiki/What's-different-in-2.0#error-handling
     */
    public static void initRxJava() {
        RxJavaPlugins.setErrorHandler(e -> {
            if (e instanceof UndeliverableException) {
                e = e.getCause();
            }
            Thread.currentThread()
                  .getUncaughtExceptionHandler()
                  .uncaughtException(Thread.currentThread(), e);
        });
        RxJavaAssemblyTracking.enable();
        RxJavaPlugins.setFailOnNonBlockingScheduler(true);
    }
}
