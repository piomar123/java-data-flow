package me.piomar.dataflow.service;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import me.piomar.dataflow.Utils;
import me.piomar.dataflow.model.NetworkDetails;

public class DetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DetailService.class);
    private static final int FAKE_INIT_MS = 150;

    public Map<String, NetworkDetails> fetchDetails(Collection<String> ssids) {
        LOGGER.info("Fetching details for {} ssids", ssids.size());
        Utils.sleep(FAKE_INIT_MS + ssids.size());
        // if (Math.random() <= 0.01) {
        //     throw new IllegalStateException("Random error");
        // }
        return ssids.stream().collect(Collectors.toMap(ssid -> ssid,
                                                       ssid -> new NetworkDetails(ssid, "admin123")));
    }
}
