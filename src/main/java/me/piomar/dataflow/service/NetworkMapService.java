package me.piomar.dataflow.service;

import static java.lang.Math.round;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import me.piomar.dataflow.Utils;
import me.piomar.dataflow.model.Envelope;

public class NetworkMapService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkMapService.class);
    private static final int FAKE_INIT_MS = 100;

    public Set<String> fetchSSIDs(Envelope scope) {
        long scopeArea = round(scope.area());
        LOGGER.info("Fetching SSIDs for area of {}", scopeArea);
        Utils.sleep(FAKE_INIT_MS + scopeArea);
        return generateIds(scope);
    }

    private Set<String> generateIds(Envelope scope) {
        Set<String> results = new HashSet<>();
        long minX = round(scope.minX());
        long maxX = round(scope.maxX());
        long minY = round(scope.minY());
        long maxY = round(scope.maxY());
        for (long x = minX; x < maxX; x++) {
            for (long y = minY; y < maxY; y++) {
                results.add(String.format("%d:%d", x, y));
            }
        }
        return results;
    }


}
