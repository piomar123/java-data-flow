package me.piomar.dataflow.model;

public class NetworkDetails {

    private final String ssid;
    private final String password;

    public NetworkDetails(String ssid, String password) {
        this.ssid = ssid;
        this.password = password;
    }

    public String ssid() {
        return ssid;
    }

    public String password() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("{%s, ****}", ssid);
    }
}
