package me.piomar.dataflow.model;

import java.util.Locale;

import com.google.common.base.Preconditions;

public class Envelope {

    private final double minX;
    private final double minY;
    private final double maxX;
    private final double maxY;

    public Envelope(double minX, double maxX, double minY, double maxY) {
        Preconditions.checkArgument(minX < maxX, "Expected %s < %s", minX, maxX);
        Preconditions.checkArgument(minY < maxY, "Expected %s < %s", minY, maxY);
        this.minX = minX;
        this.minY = minY;
        this.maxX = maxX;
        this.maxY = maxY;
    }

    public double minX() {
        return minX;
    }

    public double minY() {
        return minY;
    }

    public double maxX() {
        return maxX;
    }

    public double maxY() {
        return maxY;
    }

    public double area() {
        return (maxX - minX) * (maxY - minY);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "{%s, %s -> %s, %s}", minX, minY, maxX, maxY);
    }
}
