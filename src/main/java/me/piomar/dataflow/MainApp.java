package me.piomar.dataflow;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;

import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;

public class MainApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) {
        LOGGER.info("App start");

        Utils.initRxJava();

        List<Flow> flows = ImmutableList.of(new RxCompleteFlow(), new RxAdaptiveFlow());
        for (Flow flow : flows) {
            System.gc();
            LOGGER.info("======== {} ========", flow.getClass().getSimpleName());
            Stopwatch stopwatch = Stopwatch.createStarted();
            Map<String, NetworkDetails> detailsBySsid = flow.fetchNetworks(new Envelope(0, 1000, 0, 100));
            LOGGER.atInfo()
                  .addArgument(detailsBySsid.size())
                  .addArgument(() -> flow.getClass().getSimpleName())
                  .addArgument(() -> Utils.formatDuration(stopwatch.elapsed()))
                  .log("Fetched {} entries by {} in {}");
        }
    }

}
