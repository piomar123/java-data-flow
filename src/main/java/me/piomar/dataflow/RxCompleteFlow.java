package me.piomar.dataflow;

import static me.piomar.dataflow.Settings.BATCH_SIZE;
import static me.piomar.dataflow.Settings.TILE_SIZE;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;
import me.piomar.dataflow.service.DetailService;
import me.piomar.dataflow.service.NetworkMapService;

public class RxCompleteFlow implements Flow {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxCompleteFlow.class);

    private final NetworkMapService networkMap = new NetworkMapService();
    private final DetailService details = new DetailService();

    @Override
    public Map<String, NetworkDetails> fetchNetworks(Envelope scope) {
        List<Envelope> tiles = Utils.envelopeSplit(scope, TILE_SIZE);
        Flowable<String> ssidsFlow = Flowable.fromIterable(tiles)
                                             .observeOn(Schedulers.io())
                                             .parallel()
                                             .runOn(Schedulers.io())
                                             .map(networkMap::fetchSSIDs)
                                             .sequential()
                                             .flatMapIterable(ids -> ids)
                                             .distinct();
        return ssidsFlow
            .buffer(BATCH_SIZE)
            .parallel()
            .runOn(Schedulers.io())
            .map(details::fetchDetails)
            .sequential()
            .flatMapIterable(Map::entrySet)
            .toMap(Map.Entry::getKey, Map.Entry::getValue)
            .subscribeOn(Schedulers.single())
            .blockingGet();
    }
}
