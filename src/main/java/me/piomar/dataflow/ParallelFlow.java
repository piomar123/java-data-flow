package me.piomar.dataflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.google.common.collect.Iterables;

import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;
import me.piomar.dataflow.service.DetailService;
import me.piomar.dataflow.service.NetworkMapService;

public class ParallelFlow implements Flow {

    private static final double TILE_SIZE = 20;
    private static final int BATCH_SIZE = 500;
    private final NetworkMapService networkMap = new NetworkMapService();
    private final DetailService details = new DetailService();

    @Override
    public Map<String, NetworkDetails> fetchNetworks(Envelope scope) {
        List<Envelope> tiles = Utils.envelopeSplit(scope, TILE_SIZE);
        Set<String> ssids = tiles.parallelStream()
                                 .map(networkMap::fetchSSIDs)
                                 .flatMap(Set::stream)
                                 .collect(Collectors.toSet());

        List<List<String>> partitions = StreamSupport.stream(Iterables.partition(ssids, BATCH_SIZE).spliterator(), false)
                                                     .map(ArrayList::new)
                                                     .collect(Collectors.toList());
        return partitions.parallelStream()
                         .map(details::fetchDetails)
                         .map(Map::entrySet)
                         .flatMap(Set::stream)
                         .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
