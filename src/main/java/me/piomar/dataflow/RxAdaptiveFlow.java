package me.piomar.dataflow;

import static me.piomar.dataflow.Settings.BATCH_SIZE;
import static me.piomar.dataflow.Settings.TILE_SIZE;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.netflix.concurrency.limits.Limit;
import com.netflix.concurrency.limits.executors.BlockingAdaptiveExecutor;
import com.netflix.concurrency.limits.limit.Gradient2Limit;
import com.netflix.concurrency.limits.limit.TracingLimitDecorator;
import com.netflix.concurrency.limits.limiter.BlockingLimiter;
import com.netflix.concurrency.limits.limiter.SimpleLimiter;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;
import me.piomar.dataflow.service.DetailService;
import me.piomar.dataflow.service.NetworkMapService;

public class RxAdaptiveFlow implements Flow {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxAdaptiveFlow.class);

    private final NetworkMapService networkMap = new NetworkMapService();
    private final DetailService details = new DetailService();

    @Override
    public Map<String, NetworkDetails> fetchNetworks(Envelope scope) {
        List<Envelope> tiles = Utils.envelopeSplit(scope, TILE_SIZE);

        ExecutorService executorA = createExecutor("adaptive-A");
        BlockingAdaptiveExecutor adaptiveExecutorA = createAdaptiveExecutor(executorA);

        ExecutorService executorB = createExecutor("adaptive-B");
        BlockingAdaptiveExecutor adaptiveExecutorB = createAdaptiveExecutor(executorB);
        try {
            Scheduler adaptiveSchedulerA = Schedulers.from(adaptiveExecutorA);

            Flowable<String> ssidsFlow = Flowable.fromIterable(tiles)
                                                 .observeOn(Schedulers.io())
                                                 .flatMap(tile -> Flowable.just(tile)
                                                                          .subscribeOn(adaptiveSchedulerA)
                                                                          .map(networkMap::fetchSSIDs))
                                                 .observeOn(Schedulers.computation())
                                                 .flatMapIterable(batch -> batch)
                                                 .distinct();

            Scheduler adaptiveSchedulerB = Schedulers.from(adaptiveExecutorB);
            return ssidsFlow
                .buffer(BATCH_SIZE)
                .observeOn(Schedulers.io())
                .flatMap(batch -> Flowable.just(batch)
                                          .subscribeOn(adaptiveSchedulerB)
                                          .map(details::fetchDetails))
                .observeOn(Schedulers.computation())
                .flatMapIterable(Map::entrySet)
                .toMap(Map.Entry::getKey, Map.Entry::getValue)
                .blockingGet();
        } finally {
            executorA.shutdown();
            executorB.shutdown();
        }
    }

    private ExecutorService createExecutor(String threadPrefix) {
        return Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat(threadPrefix + "-%d").build());
    }

    private BlockingAdaptiveExecutor createAdaptiveExecutor(ExecutorService executorService) {
        Limit limit = Gradient2Limit.newBuilder().initialLimit(8).minLimit(2).maxConcurrency(100).build();
        SimpleLimiter<Void> limiter = SimpleLimiter.newBuilder().limit(TracingLimitDecorator.wrap(limit)).build();
        return BlockingAdaptiveExecutor.newBuilder()
                                       .executor(executorService)
                                       .limiter(BlockingLimiter.wrap(limiter))
                                       .build();
    }
}
