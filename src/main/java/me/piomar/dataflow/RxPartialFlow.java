package me.piomar.dataflow;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;
import me.piomar.dataflow.service.DetailService;
import me.piomar.dataflow.service.NetworkMapService;

public class RxPartialFlow implements Flow {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxPartialFlow.class);

    private final NetworkMapService networkMap = new NetworkMapService();
    private final DetailService details = new DetailService();

    @Override
    public Map<String, NetworkDetails> fetchNetworks(Envelope scope) {
        List<Envelope> tiles = Utils.envelopeSplit(scope, Settings.TILE_SIZE);
        Set<String> ssids = Flowable.fromIterable(tiles)
                                    .parallel()
                                    .runOn(Schedulers.io())
                                    .map(networkMap::fetchSSIDs)
                                    .sequential()
                                    //.doOnRequest(log request size)
                                    // .observeOn(Schedulers.computation())
                                    // .doOnNext(ids -> LOGGER.info("  batch of {}", ids.size()))
                                    .flatMapIterable(ids -> ids)
                                    .collect(Collectors.toSet())
                                    .blockingGet();

        return Flowable.fromIterable(ssids)
                       .buffer(Settings.BATCH_SIZE)
                       .parallel()
                       .runOn(Schedulers.io())
                       .map(details::fetchDetails)
                       .map(Map::entrySet)
                       .sequential()
                       .flatMapIterable(batch -> batch)
                       .toMap(Map.Entry::getKey, Map.Entry::getValue)
                       .blockingGet();
    }
}
