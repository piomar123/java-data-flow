package me.piomar.dataflow;

import java.util.Map;
import java.util.Set;

import me.piomar.dataflow.model.Envelope;
import me.piomar.dataflow.model.NetworkDetails;
import me.piomar.dataflow.service.DetailService;
import me.piomar.dataflow.service.NetworkMapService;

public class NaiveFlow implements Flow {

    private final NetworkMapService networkMap = new NetworkMapService();
    private final DetailService details = new DetailService();

    @Override
    public Map<String, NetworkDetails> fetchNetworks(Envelope scope) {
        Set<String> ssids = networkMap.fetchSSIDs(scope);
        return details.fetchDetails(ssids);
    }
}
