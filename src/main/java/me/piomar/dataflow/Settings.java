package me.piomar.dataflow;

public class Settings {

    static final double TILE_SIZE = 20;
    static final int BATCH_SIZE = 500;
}
