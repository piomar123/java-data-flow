package me.piomar.dataflow;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.akarnokd.rxjava3.debug.RxJavaAssemblyTracking;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class RxErrorApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(RxErrorApp.class);

    public static void main(String[] args) {
        LOGGER.info("App start");
        // RxJavaAssemblyTracking.enable();

        try {
            List<String> list = Flowable.just("a", "b", "c")
                                        .observeOn(Schedulers.computation())
                                        .map(RxErrorApp::bang)
                                        .toList()
                                        .blockingGet();
            LOGGER.info("{}", list);
        } catch (Exception e) {
            LOGGER.error("Error", e);
        }
    }

    static String bang(String input) {
        throw new IllegalStateException("bang");
    }
}
